import turtle

t = turtle.Turtle()
delta = 10

def squares():
    t.clear()
    t.home()
    t.forward(50)

def run_squares():
    squares()
    turtle.done()

run_squares()